<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;

require_once __DIR__ . '/../../vendor/autoload.php';
try {
    $connection = new AMQPStreamConnection(
        'localhost',
        5672,
        'guest',
        'guest'
    );
    $channel = $connection->channel();

    $channel->exchange_declare('direct_logs', 'direct', false, true, false);
    list($queue_name1, ,) = $channel->queue_declare('test_queue_1', false, true, true, false);
    list($queue_name2, ,) = $channel->queue_declare('test_queue_2', false, true, true, false);

    $severities = array_slice($argv, 1);
    if (empty($severities)) {
        file_put_contents('php://stderr', "Usage: $argv[0] [info] [warning] [error]\n");
        exit(1);
    }

    foreach ($severities as $severity) {
        $channel->queue_bind($queue_name1, 'direct_logs', $severity);
        $channel->queue_bind($queue_name2, 'direct_logs', $severity);
    }
    echo " [*] Waiting for logs. To exit press CTRL+C\n";

    $callback = function ($msg) {
        echo ' [x] ', $msg->delivery_info['routing_key'], ':', $msg->body, "\n";
    };

    $channel->basic_consume($queue_name1, '', false, true, false, false, $callback);
    $channel->basic_consume($queue_name2, '', false, true, false, false, $callback);
    while ($channel->is_open()) {
        $channel->wait();
    }
    $channel->close();
    $connection->close();
} catch (Exception $e) {
    throw new RuntimeException($e->getMessage());
}
