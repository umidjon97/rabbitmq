<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__ . '/../../vendor/autoload.php';
try {
    $connection = new AMQPStreamConnection(
        'localhost',
        5672,
        'guest',
        'guest'
    );
    $channel = $connection->channel();

    $channel->exchange_declare('direct_logs', 'direct', false, true, false);
    $severity = !empty($argv[1]) ? $argv[1] : 'info';

    $data = implode(' ', array_slice($argv, 2));
    if (empty($data)) {
        $data = 'Hello World! Routing';
    }
    $msg = new AMQPMessage(
        $data,
        [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]
    );

    $channel->basic_publish($msg, 'direct_logs', $severity);

    echo ' [x] Sent ', $severity, ':', $data, "\n";

    $channel->close();
    $connection->close();
} catch (Exception $e) {
    throw new RuntimeException($e->getMessage());
}
