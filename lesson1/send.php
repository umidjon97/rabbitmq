<?php
require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', '5672', 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('hello', false, true, false, false);
$msg = new AMQPMessage('Assalomu alaykum Umidjon');
$channel->basic_publish($msg, '', 'hello');

echo " [x] Sent 'Hello World!'\n";
$channel->close();

try {
    $connection->close();
} catch (Exception $e) {
    throw new Exception($e->getMessage());
}