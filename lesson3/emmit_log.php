<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require_once __DIR__ . '/../vendor/autoload.php';

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->exchange_declare('logs', 'fanout', false, false, false);
$msg = 'Hello Exchange! Hi Umidjon';

$message = new AMQPMessage($msg);
$channel->basic_publish($message, 'logs');

echo ' [x] Sent ', $msg, "\n";

$channel->close();
$connection->close();
